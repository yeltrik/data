<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\People\app\http\controllers\PersonController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('data/{data}',
    [\Yeltrik\Data\app\http\controllers\DataController::class, 'show'])
    ->name('data.show');

Route::post('data',
    [\Yeltrik\Data\app\http\controllers\DataController::class, 'store'])
    ->name('data.store');
