{{--For Testing--}}
{{--<?php $model = new \Yeltrik\FooBar\app\models\Foo()?>--}}
{{--<?php $model->save() ?>--}}

@if( !isset($model) )
    {{dd('Model must be provided to associate Data') }}
@else
    <?php $interpreting_class_name = get_class($model) ?>
    <?php $model_id = $model->getKey() ?>
@endif

<form
    method="POST"
    action="{{ action([\Yeltrik\Data\app\http\controllers\DataController::class, 'store']) }}"
>
    @csrf

    <input type="hidden" name="interpreting_class_name" value="{{$interpreting_class_name}}"> <br>
    <input type="hidden" name="model_id" value="{{$model_id}}"> <br>

    <button class="btn btn-info">
        Create Data
    </button>
</form>
