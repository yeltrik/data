<?php

namespace Yeltrik\Data\app\traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Yeltrik\Data\app\models\Data;

trait HasData
{

    /**
     * @return Builder[]|Collection
     */
    public static function allData()
    {
        return static::dataQuery()->get();
    }

    /**
     * @return Builder[]|Collection
     */
    public function data()
    {
        $query = static::dataQuery();

        if ($this->getId() != NULL) {
            $query->where('model_id', '=', $this->getId());
        }

        return $query->get();
    }

    /**
     * @return Builder
     */
    public static function dataQuery()
    {
        return Data::query()
            ->where('interpreting_class_name', '=', get_called_class());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        if ($this->id == NULL) {
            $this->save();
        }
        return $this->id;
    }

    /**
     * @param array $datumData
     * @return Data
     */
    public function setData(array $datumData): Data
    {
        $data = Data::setData(get_called_class(), $datumData);
        if ($this->getId() != NULL) {
            $data->model_id = $this->getId();
            $data->save();
        }
        return $data;
    }

}
