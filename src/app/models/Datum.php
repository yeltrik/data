<?php

namespace Yeltrik\Data\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class Datum
 *
 * @property int id
 * @property int data_id
 * @property string key
 * @property mixed value
 * @property string type
 *
 * @property Data data
 *
 * @package Yeltrik\Data\app\models
 */
class Datum extends Model
{
    use HasFactory;
    use RevisionableTrait;
    use SoftDeletes;

    protected $connection = 'data';
    public $table = 'datum';

    protected $revisionCreationsEnabled = true;

    /**
     * @return BelongsTo
     */
    public function data()
    {
        return $this->belongsTo(Data::class);
    }

}
