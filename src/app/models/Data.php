<?php

namespace Yeltrik\Data\app\models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * Class Data
 *
 * @property int id
 * @property string interpreting_class_name
 * @property int model_id
 *
 * @property Collection datum
 *
 * @package Yeltrik\Data\app\models
 */
class Data extends Model
{
    use HasFactory;
    use RevisionableTrait;
    use SoftDeletes;

    protected $connection = 'data';
    public $table = 'data';

    protected $revisionCreationsEnabled = true;

    /**
     * @return HasMany
     */
    public function datum()
    {
        return $this->hasMany(Datum::class);
    }

    /**
     * @param string $key
     * @param $value
     * @param bool $forceValueType
     * @return bool
     */
    private function datumExists(string $key, $value, bool $forceValueType = TRUE): bool
    {
        return $this->datumQuery($key, $value, $forceValueType)->exists();
    }

    /**
     * @param string $key
     * @param $value
     * @param bool $forceValueType
     * @return Builder
     */
    private function datumQuery(string $key, $value, bool $forceValueType = TRUE): Builder
    {
        $datumQuery = Datum::query()
            ->where('data_id', '=', $this->getId())
            ->where('key', '=', $key)
            ->where('value', '=', $value);

        if ($forceValueType) {
            $datumQuery->where('type', '=', gettype($value));
        }

        return $datumQuery;
    }

    /**
     * @return int
     */
    public function getId()
    {
        if ($this->id == NULL) {
            $this->save();
        }
        return $this->id;
    }

    /**
     * @return mixed|null
     */
    public function model()
    {
        $classname = $this->interpreting_class_name;
        if (class_exists($classname)) {
            if ($this->model_id != NULL) {
                return $classname::query()
                    ->where('id', '=', $this->model_id)
                    ->first();
            } else {
                return new $classname();
            }
        } else {
            return NULL;
        }
    }

    /**
     * @param string $interpreting_class_name
     * @param array $datumData
     * @return Data
     */
    public static function setData(string $interpreting_class_name, array $datumData)
    {
        $data = new Data();
        $data->interpreting_class_name = $interpreting_class_name;
        $data->save();
        foreach ($datumData as $key => $value) {
            $data->setDatum($key, $value);
        }
        return $data;
    }

    /**
     * @param string $key
     * @param $value
     * @return Datum|null
     */
    public function setDatum(string $key, $value): ?Datum
    {
        if (!$this->datumExists($key, $value)) {
            $datum = new Datum();
            $datum->data()->associate($this);
            $datum->key = $key;
            $datum->value = $value;
            $datum->type = gettype($value);
            $datum->save();
            return $datum;
        } else {
            return NULL;
        }
    }

}
