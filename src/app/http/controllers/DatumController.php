<?php

namespace Yeltrik\Data\app\http\controllers;

use App\Http\Controllers\Controller;
use Yeltrik\Data\app\models\Datum;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DatumController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Datum $datum
     * @return Response
     */
    public function show(Datum $datum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Datum $datum
     * @return Response
     */
    public function edit(Datum $datum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Datum $datum
     * @return Response
     */
    public function update(Request $request, Datum $datum)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Datum $datum
     * @return Response
     */
    public function destroy(Datum $datum)
    {
        //
    }
}
