<?php

namespace Yeltrik\Data\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Yeltrik\Data\app\models\Data;
use Illuminate\Http\Request;

class DataController extends Controller
{
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $result = $request->all();
        $interpreting_class_name = $result['interpreting_class_name'];
        $model_id = $result['model_id'];

        $data = new Data();
        $data->interpreting_class_name = $interpreting_class_name;
        $data->model_id = $model_id;
        $data->save();

        return redirect()->route('data.show', [$data]);
    }

    /**
     * Display the specified resource.
     *
     * @param Data $data
     * @return void
     */
    public function show(Data $data)
    {
        return view('data::data.show', compact(
            'data'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Data $data
     * @return void
     */
    public function edit(Data $data)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Data $data
     * @return void
     */
    public function update(Request $request, Data $data)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Data $data
     * @return void
     */
    public function destroy(Data $data)
    {
        //
    }
}
