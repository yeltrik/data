<?php

use Illuminate\Support\Str;

return

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    [
        // Connection
        'data' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('Data_DB_DATABASE', database_path('data.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // SQLITE
        'data_sqlite_example' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('Data_DB_DATABASE', database_path('data.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        // MYSQL
        'data_mysql_example' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('Data_DB_HOST', '127.0.0.1'),
            'port' => env('Data_DB_PORT', '3306'),
            'database' => env('Data_DB_DATABASE', 'data'),
            'username' => env('Data_DB_USERNAME', 'data'),
            'password' => env('Data_DB_PASSWORD', ''),
            'unix_socket' => env('Data_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

    ];
